FROM centos:7

# test
#RUN yum update -y
#RUN yum groupinstall "Development Tools" -y
#RUN yum install wget zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel zlib* libffi-devel readline-devel tk-devel -y

# https://help.dreamhost.com/hc/en-us/articles/360001435926-Installing-OpenSSL-locally-under-your-username
#RUN wget https://www.openssl.org/source/openssl-1.1.1g.tar.gz --no-check-certificate
#RUN tar zxvf openssl-1.1.1g.tar.gz
#RUN cd openssl-1.1.1g
#RUN ./openssl-1.1.1g/config --prefix=/home/root/openssl --openssldir=/home/root/openssl no-ssl2
#RUN make install
#RUN cd ~
#RUN export PATH=$HOME/openssl/bin:$PATH
#RUN export LD_LIBRARY_PATH=$HOME/openssl/lib
#RUN export LC_ALL="en_US.UTF-8"
#RUN export LDFLAGS="-L /home/root/openssl/lib -Wl,-rpath,/home/root/openssl/lib"
#RUN . ~/.profile



#RUN wget https://www.python.org/ftp/python/3.9.1/Python-3.9.1.tgz
#RUN tar xvzf Python-3.9.1.tgz
#RUN ./Python-3.9.1/configure --with-openssl=/home/root/openssl
#RUN make install

#RUN wget https://www.python.org/ftp/python/3.10.0/Python-3.10.0.tgz
#RUN tar xvf Python-3.10.0.tgz
#RUN cd Python-3.10.0
#RUN ./configure --enable-optimizations
#RUN make altinstall

#RUN python3.9 --version
#RUN pip3.9 --version

#COPY requirements.txt requirements.txt
#RUN pip3.9 install --trusted-host pypi.python.org --trusted-host files.pythonhosted.org --trusted-host pypi.org -r requirements.txt


RUN yum install python3 python3-pip -y 
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY /python_api/python-api.py python-api.py

CMD ["python3", "python-api.py"]